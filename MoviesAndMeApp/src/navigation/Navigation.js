import React from 'react';
import {StyleSheet, Image} from 'react-native';
import {createStackNavigator, createAppContainer, createBottomTabNavigator} from 'react-navigation';

import SearchScreen from '../screens/SearchScreen';
import FilmDetailScreen from '../screens/FilmDetailScreen';
import FavoritesScreen from '../screens/FavoritesScreen';
import NewsScreen from '../screens/NewsScreen';
import SeenScreen from '../screens/SeenScreen';


const SearchStackNavigator = createStackNavigator({
    SearchScreen: {
        screen: SearchScreen,
        navigationOptions: {
            title: 'Rechercher',
        },
    },
    FilmDetailScreen: {
        screen: FilmDetailScreen,
        navigationOptions: {
            title: 'Détail',
        },
    },
});

const FavoritesStackNavigator = createStackNavigator({
    FavoritesScreen: {
        screen: FavoritesScreen,
        navigationOptions: {
            title: 'Favoris',
        },
    },
    FilmDetailScreen: {
        screen: FilmDetailScreen,
        navigationOptions: {
            title: 'Détail',
        },
    },
});

const NewsStackNavigator = createStackNavigator({
    NewsScreen: {
        screen: NewsScreen,
        navigationOptions: {
            title: 'Les nouveautés',
        },
    },
    FilmDetailScreen: {
        screen: FilmDetailScreen,
        navigationOptions: {
            title: 'Détail',
        },
    },
});

const SeenStackNavigator = createStackNavigator({
    SeenScreen: {
        screen: SeenScreen,
        navigationOptions: {
            title: 'Les films vus',
        },
    },
    FilmDetailScreen: {
        screen: FilmDetailScreen,
        navigationOptions: {
            title: 'Détail',
        },
    },
});

const MoviesTabNavigator = createBottomTabNavigator(
    {
        Search: {
            screen: SearchStackNavigator,
            navigationOptions: {
                tabBarIcon: () => { // On définit le rendu de nos icônes par les images récemment ajoutés au projet
                    return <Image
                        source={require('../../assets/ic_search.png')}
                        style={styles.icon}/>; // On applique un style pour les redimensionner comme il faut
                },
            },
        },
        Favorites: {
            screen: FavoritesStackNavigator,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Image
                        source={require('../../assets/ic_favorite.png')}
                        style={styles.icon}/>;
                },
            },
        },
        News: {
            screen: NewsStackNavigator,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Image
                        source={require('../../assets/ic_fiber_new.png')}
                        style={styles.icon}/>;
                },
            },
        },
        Seen: {
            screen: SeenStackNavigator,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Image
                        source={require('../../assets/ic_seen.png')}
                        style={styles.icon}/>;
                },
            },
        },
    },
    {
        tabBarOptions: {
            activeBackgroundColor: '#DDDDDD', // Couleur d'arrière-plan de l'onglet sélectionné
            inactiveBackgroundColor: '#FFFFFF', // Couleur d'arrière-plan des onglets non sélectionnés
            showLabel: false, // On masque les titres
            showIcon: true, // On informe le TabNavigator qu'on souhaite afficher les icônes définis
        },
    },
);

const styles = StyleSheet.create({
    icon: {
        width: 30,
        height: 30,
    },
});

export default createAppContainer(MoviesTabNavigator);
